<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class QuestionController extends AbstractController
{
    public const ROUTE_QUESTION = 'question';
    public const QUESTION_INPUT = 'questionId';

    /**
     * @Route("/question", name=\App\Controller\QuestionController::ROUTE_QUESTION, methods={"POST"})
     * @param Request $request
     *
     * @return Response
     * @throws InternalErrorException
     */
    public function sendQuestion(Request $request): Response
    {

        $input = json_decode($request->getContent());
        $questionId = $input->{'questionId'};
        $response = array('answerId' => $questionId);

        return $this->json($response);
    }

}
